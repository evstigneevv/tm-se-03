package ru.evstigneev.tm.command;

import ru.evstigneev.tm.entity.Project;
import ru.evstigneev.tm.entity.Task;
import ru.evstigneev.tm.service.ProjectService;
import ru.evstigneev.tm.service.TaskService;

import java.util.List;
import java.util.Scanner;

public class Menu {

    private Scanner scanner;
    private ProjectService projectService;
    private TaskService taskService;
    private String input;
    private Project currentProject;

    public Menu(Scanner scanner, ProjectService projectService, TaskService taskService) {
        this.scanner = scanner;
        this.projectService = projectService;
        this.taskService = taskService;
    }

    public String getInput() {
        return input;
    }

    public void setInput() {
        this.input = scanner.nextLine();
    }

    public void showGreetings() {
        System.out.println("Welcome to Task Manager!");
        System.out.print("input command or \"exit\" to exit...");
    }

    public void showHelp() {
        System.out.println("List of commands: ");
        System.out.println("help - show available commands");
        System.out.println("cp - create new project");
        System.out.println("ct - create new task for current project");
        System.out.println("st - show current project's tasks");
        System.out.println("spl - show project list");
        System.out.println("sp - show current project");
        System.out.println("swpr - switch project");
        System.out.println("dp - delete project by name");
        System.out.println("up - update current project");
        System.out.println("ut - update current project's task name");
        System.out.println("dt - delete current project's task");
        System.out.println("sat - show all tasks");
        System.out.println("exit - close application");
    }

    public void createProject() {
        System.out.print("input new project name: ");
        input = scanner.nextLine();
        currentProject = projectService.createProject(input);
        System.out.println("project \"" + currentProject.getName() + " \" created ");

    }

    public void createTask() {
        System.out.println("input new task name into current project: ");
        input = scanner.nextLine();
        System.out.println("task \"" + taskService.addTask(input,
                currentProject.getId()).getName() + " \" created ");
    }

    public void showTaskList() {
        showProjectList();
        System.out.println("input project ID: ");
        input = scanner.nextLine();
        List<Task> taskList = taskService.getTaskListByProjectId(input);
        if (taskList.isEmpty()) {
            System.out.println("No tasks for project");
        } else {
            for (Task t : taskList) {
                System.out.println(t.getTaskId() + " - task ID | " + t.getName() + " - task name");
            }
        }
    }

    public void showCurrentProject() {
        if (currentProject == null) {
            System.out.println("no project selected!");
        } else {
            System.out.println("Current project is: " + currentProject.getName() + " id: " +
                    currentProject.getId());
        }
    }

    public void switchCurrentProject() {
        showProjectList();
        System.out.println("input project name: ");
        input = scanner.nextLine();
        List<Project> projectList = projectService.getProjectList();
        for (Project project : projectList) {
            if (input.equals(project.getName())) {
                currentProject = project;
            }
        }
        System.out.println("current project is " + currentProject.getName());
    }

    public void deleteProject() {
        showProjectList();
        System.out.println("input project ID: ");
        input = scanner.nextLine();
        if (projectService.deleteProject(input)) {
            System.out.println("project deleted!");
            currentProject = null;
        } else {
            System.out.println("Project wasn't deleted");
        }
    }

    public void updateProject() {
        System.out.println("input project name");
        input = scanner.nextLine();
        System.out.println("input project new name");
        projectService.updateProject(input, scanner.nextLine());
    }

    public void updateTask() {
        taskService.updateTaskName(scanner.nextLine());
    }

    public void deleteTask() {
        System.out.println("Input task name: ");
        input = scanner.nextLine();
        taskService.deleteTask(input);
        System.out.println("Task deleted!");
    }

    public boolean exit() {
        return input.equals("exit");
    }

    public void showProjectList() {
        List<Project> projectList = projectService.getProjectList();
        if (!projectList.isEmpty()) {
            for (Project project : projectList)
                System.out.println("Project uuid: " + project.getId() + " | Project name: " + project.getName());
        } else {
            System.out.println("There are no projects!");
        }
    }

    public void showAllTasks() {
        if (taskService.getTaskList().isEmpty()) {
            System.out.println("There are no tasks!");
        } else {
            for (Task t : taskService.getTaskList()) {
                System.out.println(t.getProjectId() + " - project ID | " + t.getTaskId() + " - task ID | "
                        + t.getName() + " - task name");
            }
        }

    }

}
