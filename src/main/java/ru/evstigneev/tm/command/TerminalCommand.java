package ru.evstigneev.tm.command;

public class TerminalCommand {

    public static final String HELP = "HELP";
    public static final String CREATE_PROJECT = "CP";
    public static final String CREATE_TASK = "CT";
    public static final String SHOW_TASK = "ST";
    public static final String SHOW_PROJECT_LIST = "SPL";
    public static final String SHOW_CURRENT_PROJECT = "SP";
    public static final String SWITCH_CURRENT_PROJECT = "SWPR";
    public static final String DELETE_PROJECT = "DP";
    public static final String UPDATE_PROJECT = "UP";
    public static final String UPDATE_TASK = "UT";
    public static final String DELETE_TASK = "DT";
    public static final String SHOW_ALL_TASKS = "SAT";

}
