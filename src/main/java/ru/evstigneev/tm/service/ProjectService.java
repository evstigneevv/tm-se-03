package ru.evstigneev.tm.service;

import ru.evstigneev.tm.entity.Project;
import ru.evstigneev.tm.repository.ProjectRepository;
import ru.evstigneev.tm.repository.TaskRepository;

import java.util.List;

public class ProjectService {

    private ProjectRepository projectRepository;
    private TaskRepository taskRepository;


    public ProjectService(ProjectRepository projectRepository, TaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    public List<Project> getProjectList() {
        return projectRepository.getProjectList();
    }

    public Project createProject(String projectName) {
        return projectRepository.createProject(projectName);
    }

    public boolean deleteProject(String projectId) {
        taskRepository.deleteAllProjectTasks(projectId);
        return projectRepository.deleteProject(projectId);
    }

    public void updateProject(String projectName, String newProjectName) {
        projectRepository.updateProject(projectName, newProjectName);
    }

    public void setProjectList(List<Project> projectList) {
        projectRepository.setProjectList(projectList);
    }

}
