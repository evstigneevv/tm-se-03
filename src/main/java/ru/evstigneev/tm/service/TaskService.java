package ru.evstigneev.tm.service;

import ru.evstigneev.tm.entity.Task;
import ru.evstigneev.tm.repository.TaskRepository;

import java.util.List;

public class TaskService {

    private TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public Task addTask(String taskName, String projectId) {
        return taskRepository.addTask(taskName, projectId);
    }

    public List<Task> getTaskList() {
        return taskRepository.getTaskList();
    }

    public void setTaskList(List<Task> taskList) {
        taskRepository.setTaskList(taskList);
    }

    public void updateTaskName(String taskName) {
        taskRepository.updateTask(taskName);
    }

    public void deleteTask(String taskName) {
        taskRepository.deleteTask(taskName);
    }

    public List<Task> getTaskListByProjectId(String projectId) {
        return taskRepository.getTaskListByProjectId(projectId);
    }
}
