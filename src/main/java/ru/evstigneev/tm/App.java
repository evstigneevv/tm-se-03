package ru.evstigneev.tm;

import ru.evstigneev.tm.command.Menu;
import ru.evstigneev.tm.command.TerminalCommand;
import ru.evstigneev.tm.repository.ProjectRepository;
import ru.evstigneev.tm.repository.TaskRepository;
import ru.evstigneev.tm.service.ProjectService;
import ru.evstigneev.tm.service.TaskService;

import java.util.Scanner;

public class App {

    public static void main(String[] args) {
        TaskRepository taskRepository = new TaskRepository();
        Menu menu = new Menu(new Scanner(System.in), new ProjectService(new ProjectRepository(), taskRepository),
                new TaskService(taskRepository));
        menu.showGreetings();
        do {
            menu.setInput();
            switch (menu.getInput().toUpperCase()) {
                case TerminalCommand.HELP:
                    menu.showHelp();
                    break;
                case TerminalCommand.CREATE_PROJECT:
                    menu.createProject();
                    break;
                case TerminalCommand.CREATE_TASK:
                    menu.createTask();
                    break;
                case TerminalCommand.SHOW_TASK:
                    menu.showTaskList();
                    break;
                case TerminalCommand.SHOW_PROJECT_LIST:
                    menu.showProjectList();
                    break;
                case TerminalCommand.SHOW_CURRENT_PROJECT:
                    menu.showCurrentProject();
                    break;
                case TerminalCommand.SWITCH_CURRENT_PROJECT:
                    menu.switchCurrentProject();
                    break;
                case TerminalCommand.DELETE_PROJECT:
                    menu.deleteProject();
                    break;
                case TerminalCommand.UPDATE_PROJECT:
                    menu.updateProject();
                    break;
                case TerminalCommand.UPDATE_TASK:
                    menu.updateTask();
                    break;
                case TerminalCommand.DELETE_TASK:
                    menu.deleteTask();
                    break;
                case TerminalCommand.SHOW_ALL_TASKS:
                    menu.showAllTasks();
                    break;
            }
        }
        while (!menu.exit());
    }

}
