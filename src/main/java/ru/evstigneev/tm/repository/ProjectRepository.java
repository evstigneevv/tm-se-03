package ru.evstigneev.tm.repository;

import ru.evstigneev.tm.entity.Project;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.UUID;

public class ProjectRepository {

    private List<Project> projectList = new ArrayList<>();

    public Project createProject(String projectName) {
        Project project = new Project(projectName, UUID.randomUUID().toString());
        projectList.add(project);
        return project;
    }

    public boolean deleteProject(String projectId) {
        ListIterator<Project> listIterator = projectList.listIterator();
        while (listIterator.hasNext()) {
            if (projectId.equals(listIterator.next().getId())) {
                listIterator.remove();
                return true;
            }
        }
        return false;
    }

    public void updateProject(String projectName, String newProjectName) {
        for (Project project : projectList) {
            if (projectName.equals(project.getName())) {
                project.setName(newProjectName);
            }
        }
    }

    public List<Project> getProjectList() {
        return projectList;
    }

    public void setProjectList(List<Project> projectList) {
        this.projectList = projectList;
    }
}
