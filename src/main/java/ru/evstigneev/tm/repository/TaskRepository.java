package ru.evstigneev.tm.repository;

import ru.evstigneev.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.UUID;

public class TaskRepository {

    private List<Task> taskList = new ArrayList<>();

    public Task addTask(String taskName, String projectId) {
        Task newTask = new Task(taskName, UUID.randomUUID().toString(), projectId);
        taskList.add(newTask);
        return newTask;
    }

    public List<Task> getTaskListByProjectId(String projectId) {
        List<Task> taskListByProjectId = new ArrayList<>();
        for (Task t : taskList) {
            if (t.getProjectId().equals(projectId)) {
                taskListByProjectId.add(t);
            }
        }
        return taskListByProjectId;
    }

    public List<Task> getTaskList() {
        return taskList;
    }

    public void setTaskList(List<Task> taskList) {
        this.taskList = taskList;
    }

    public void updateTask(String taskName) {
        for (Task t : taskList) {
            if (t.getName().equals(taskName)) {
                t.setName(taskName);
            }
        }
    }

    public boolean deleteAllProjectTasks(String projectId) {
        boolean isDeleted = false;
        ListIterator<Task> listIterator = taskList.listIterator();
        while (listIterator.hasNext()) {
            if (projectId.equals(listIterator.next().getProjectId())) {
                listIterator.remove();
                isDeleted = true;
            }
        }
        return isDeleted;
    }

    public boolean deleteTask(String taskName) {
        ListIterator<Task> listIterator = taskList.listIterator();
        while (listIterator.hasNext()) {
            if (taskName.equals(listIterator.next().getName())) {
                listIterator.remove();
                return true;
            }
        }
        return false;
    }
}